package pl.djsdpjee.service

import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import pl.djsdpjee.dao.AuthorDao
import pl.djsdpjee.domain.Authors
import pl.djsdpjee.exception.AuthorNotFound
import pl.djsdpjee.repository.Author
import java.util.*
import kotlin.NoSuchElementException

object AuthorService {

    fun createAuthor(authorFirstName: String, authorLasName: String): AuthorDao{
        return transaction {
            val author = Author.new {
                firstName = authorFirstName
                lastName = authorLasName
            }
            mapAuthorToAuthorDao(author)
        }
    }

    fun findAuthor(authorFirstName: String, authorLasName: String):AuthorDao{
        return transaction {
            val authors = Author.find {
                Authors.firstName eq authorFirstName and (Authors.lastName eq authorLasName)
            }
            try {
                mapAuthorToAuthorDao(authors.first())
            } catch (e: NoSuchElementException) {
                throw AuthorNotFound("author not found")
            }
        }
    }

    internal fun getAuthorById(id: UUID): Author{
        return transaction {
            Author.findById(id) ?: throw AuthorNotFound("author with $id not found")
        }
    }

    internal fun mapAuthorsListToAuthorsDaoList(authors: SizedIterable<Author>?): List<AuthorDao> {
        return authors?.map { author -> mapAuthorToAuthorDao(author) }?.toList() ?: listOf()
    }

    private fun mapAuthorToAuthorDao(author: Author): AuthorDao{
        return AuthorDao(author.id.value, author.firstName, author.lastName)
    }
}
