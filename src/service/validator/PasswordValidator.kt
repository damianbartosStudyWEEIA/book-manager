package pl.djsdpjee.service.validator

sealed class PasswordValidatorResult {
    object Success : PasswordValidatorResult()
    data class Error(val errors: List<String>) : PasswordValidatorResult()
}

object PasswordValidator{

    fun validatePassword(password: String, rePassword: String): PasswordValidatorResult {
        val errorList = mutableListOf<String>()

        errorList.addAll(validatePasswordMatch(password, rePassword))
        errorList.addAll(validatePassword(password))

        return if(errorList.size>0){
            PasswordValidatorResult.Error(errorList)
        }else{
            PasswordValidatorResult.Success
        }
    }

    private fun validatePasswordMatch(password: String, rePassword:String): List<String> {
        val errorList = mutableListOf<String>()
        if(password != rePassword){
            errorList.add("password does not match")
        }
        return errorList
    }

    private fun validatePassword(password: String) : List<String> {
        val errorList = mutableListOf<String>()
        if (password.length < 5) {
            errorList.add("password must contain minimum 5 characters")
        }
        if (getUpperLetterCount(password) < 1) {
            errorList.add("password must contain minimum 1 upper letter")
        }
        if (getSpecialLetterCount(password) < 1) {
            errorList.add("password must contain minimum 1 special letter")
        }
        return errorList
    }

    private fun getUpperLetterCount(input: String): Int {
        var upperLetterCounter = 0
        for (char in input) {
            if (char.isUpperCase()) {
                upperLetterCounter++
            }
        }
        return upperLetterCounter
    }

    private fun getSpecialLetterCount(input: String): Int {
        var specialLetterCount = 0
        for (char in input) {
            if (!char.isLetter() && !char.isDigit()) {
                specialLetterCount++
            }
        }
        return specialLetterCount
    }

}
