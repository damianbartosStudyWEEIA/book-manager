package pl.djsdpjee.service.validator

import pl.djsdpjee.service.UserService

sealed class UserValidatorResult {
    object Success : UserValidatorResult()
    data class Error(val errors: List<String>) : UserValidatorResult()
}

object UserValidator {

    fun validateUser(login: String, password: String, rePassword:String): UserValidatorResult{
        val errorList = mutableListOf<String>()

        errorList.addAll(validateUserLogin(login))
        when (val validatePassword = PasswordValidator.validatePassword(password, rePassword)) {
            is PasswordValidatorResult.Error -> errorList.addAll(validatePassword.errors)
        }

        return if(errorList.size > 0){
            UserValidatorResult.Error(errorList)
        }else{
            UserValidatorResult.Success
        }
    }

    private fun validateUserLogin(login: String): List<String> {
        val errorList = mutableListOf<String>()
        if (UserService.checkIfLoginExist(login)) {
            errorList.add("login is already used")
        }
        return errorList
    }
}

