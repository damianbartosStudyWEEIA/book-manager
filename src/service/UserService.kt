package pl.djsdpjee.service

import org.jetbrains.exposed.dao.load
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt
import pl.djsdpjee.dao.BookDao
import pl.djsdpjee.dao.UserDao
import pl.djsdpjee.domain.Users
import pl.djsdpjee.exception.LoginAlreadyExist
import pl.djsdpjee.exception.NoSuchUserException
import pl.djsdpjee.exception.PasswordChangeException
import pl.djsdpjee.repository.User
import java.util.*

object UserService {

    fun getUserDaoByLogin(userLogin: String): UserDao {
        return transaction {
            val user = getUserByLogin(userLogin)
            mapUserToDao(user)
        }
    }

    fun loginUser(login: String, password: String): Boolean {
        val user = getUserByLogin(login)
        return checkPassword(password, user.hashedPassword)
    }

    fun registerUser(login: String, password: String, rePassword: String): UserDao {
        if (checkIfLoginExist(login)) {
            throw LoginAlreadyExist("login already exist")
        }
        return transaction {
            val user = User.new {
                userLogin = login
                hashedPassword = hashPassword(password)
            }
            mapUserToDao(user)
        }
    }

    fun changePassword(login: String, password: String, newPassword: String, newPasswordRepeated: String) {
        val user = getUserByLogin(login)
        when {
            newPassword != newPasswordRepeated -> throw PasswordChangeException("password and repeat password not matched")
            !checkPassword(password, user.hashedPassword) -> throw PasswordChangeException("wrong current password")
            else -> transaction {
                user.hashedPassword = hashPassword(newPassword)
            }
        }
    }

    fun addBooksToCollection(login: String, vararg books: BookDao) {
        transaction {
            val user = getUserByLogin(login).load(User::books)
            val newBooksList = user.books.toMutableList()
            for (book in books) {
                newBooksList.add(BookService.getBookById(book.id))
            }
            user.books = SizedCollection(newBooksList)
        }
    }

    fun removeBookWithIdOwnedByUser(bookId: UUID, login:String){
        transaction {
            val user = getUserByLogin(login).load(User::books)
            val newBooksList = user.books.toMutableList()
            newBooksList.removeIf { t -> t.id.value == bookId }
            user.books = SizedCollection(newBooksList)
        }
    }

    fun bookWithIdOwnedByUser(bookId: UUID, login:String): Boolean{
        val user = getUserDaoByLogin(login)
        for (book in user.books) {
            if(book.id == bookId){
                return true
            }
        }
        return false
    }

    internal fun getUserByLogin(userLogin: String): User {
        return transaction {
            try {
                User.find { Users.userLogin eq userLogin }.first()
            } catch (e: Exception) {
                throw NoSuchUserException("no user with $userLogin found")
            }
        }
    }

    internal fun checkIfLoginExist(login: String): Boolean {
        try {
            getUserByLogin(login)
        } catch (e: NoSuchUserException) {
            return false
        }
        return true
    }

    private fun mapUserToDao(user: User): UserDao {
        return UserDao(user.userLogin, user.hashedPassword, BookService.mapBooksListToBooksDaoList(user.books))
    }

    private fun hashPassword(password: String): String {
        return BCrypt.hashpw(password, BCrypt.gensalt())
    }

    private fun checkPassword(candidate: String, hashed: String): Boolean {
        return BCrypt.checkpw(candidate, hashed)
    }
}
