package pl.djsdpjee.service

import kotlin.random.Random

object IsbnService {
    fun getRandomIsbn(): String {
        val number:MutableList<String> = mutableListOf()
        for (i in 1..13){
            number.add(Random.nextInt(0, 9).toString())
        }
        return number.joinToString("")
    }
}
