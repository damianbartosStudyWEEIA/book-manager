package pl.djsdpjee.service

import org.jetbrains.exposed.dao.load
import org.jetbrains.exposed.dao.with
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import pl.djsdpjee.dao.AuthorDao
import pl.djsdpjee.dao.BookDao
import pl.djsdpjee.exception.AuthorNotFound
import pl.djsdpjee.exception.BookNotFound
import pl.djsdpjee.repository.Author
import pl.djsdpjee.repository.Book
import pl.djsdpjee.repository.User
import java.util.*

object BookService {

    fun createBook(
        bookName: String,
        bookIsbn: String,
        bookDescription: String = "",
        bookPageNumber: Int = 0,
        bookReleaseDate: DateTime = DateTime.now(),
        bookImgUr: String = "",
        vararg bookAuthors: AuthorDao
    ): BookDao {
        return transaction {
            val newAuthorsList = mutableListOf<Author>()
            for (bookAuthor in bookAuthors) {
                newAuthorsList.add(AuthorService.getAuthorById(bookAuthor.id))
            }
            val book = Book.new {
                name = bookName
                description = bookDescription
                pageNumber = bookPageNumber
                isbn = bookIsbn
                releaseDate = bookReleaseDate
                imgUrl = bookImgUr
                authors = SizedCollection(newAuthorsList)
            }
            mapBookToBookDao(book)
        }
    }

    fun createBook(bookDao: BookDao): BookDao {
        return transaction {
            val newAuthorsList = mutableListOf<Author>()
            for (bookAuthor in bookDao.authors) {
                try {
                    newAuthorsList.add(AuthorService.getAuthorById(bookAuthor.id))
                } catch (e: AuthorNotFound) {
                    val newAuthor = AuthorService.createAuthor(bookAuthor.firstName, bookAuthor.lastName)
                    newAuthorsList.add(AuthorService.getAuthorById(newAuthor.id))
                }
            }
            val book = Book.new {
                name = bookDao.name
                description = bookDao.description
                pageNumber = bookDao.pageNumber
                isbn = bookDao.isbn
                releaseDate = bookDao.releaseDate
                imgUrl = bookDao.imgUrl
                authors = SizedCollection(newAuthorsList)
            }
            mapBookToBookDao(book)
        }
    }

    fun editBook(bookDao: BookDao): BookDao {
        return transaction {
            val book: Book = getBookById(bookDao.id)

            val newAuthorsList = mutableListOf<Author>()
            for (bookAuthor in bookDao.authors) {
                try {
                    newAuthorsList.add(AuthorService.getAuthorById(bookAuthor.id))
                } catch (e: AuthorNotFound) {
                    val newAuthor = AuthorService.createAuthor(bookAuthor.firstName, bookAuthor.lastName)
                    newAuthorsList.add(AuthorService.getAuthorById(newAuthor.id))
                }
            }

            book.name = bookDao.name
            book.description = bookDao.description
            book.pageNumber = bookDao.pageNumber
            book.isbn = bookDao.isbn
            book.releaseDate = bookDao.releaseDate
            book.imgUrl = bookDao.imgUrl
            book.authors = SizedCollection(newAuthorsList)

            mapBookToBookDao(book)
        }
    }

    fun editBook(
        bookId: UUID,
        bookName: String,
        bookIsbn: String,
        bookDescription: String = "",
        bookPageNumber: Int = 0,
        bookReleaseDate: DateTime = DateTime.now(),
        bookImgUr: String = "",
        vararg bookAuthors: AuthorDao
    ): BookDao {
        return transaction {
            val book: Book = getBookById(bookId)

            val newAuthorsList = mutableListOf<Author>()
            for (bookAuthor in bookAuthors) {
                newAuthorsList.add(AuthorService.getAuthorById(bookAuthor.id))
            }

            book.name = bookName
            book.description = bookDescription
            book.pageNumber = bookPageNumber
            book.isbn = bookIsbn
            book.releaseDate = bookReleaseDate
            book.imgUrl = bookImgUr
            book.authors = SizedCollection(newAuthorsList)

            mapBookToBookDao(book)
        }
    }

    fun getAllBooksDao(): List<BookDao> {
        return transaction {
            Book.all().with(Book::authors).map { book -> mapBookToBookDao(book) }
        }
    }

    fun getBooksDaoByUserLogin(userLogin: String): List<BookDao> {
        return transaction {
            val user = UserService.getUserByLogin(userLogin)
            val books = user.load(User::books, Book::authors).books
            books.map { mapBookToBookDao(it) }
        }
    }

    fun getBookDaoById(id: UUID): BookDao {
        return transaction {
            val book = getBookById(id)
            mapBookToBookDao(book)
        }
    }

    fun removeBookById(id: UUID){
        transaction {
            val book = getBookById(id)
            book.authors = SizedCollection()
            book.delete()
        }
    }

    internal fun getBookById(id: UUID): Book {
        return transaction {
            Book.findById(id) ?: throw BookNotFound("book not found")
        }
    }

    internal fun mapBooksListToBooksDaoList(books: SizedIterable<Book>?): List<BookDao> {
        return books?.map { book -> mapBookToBookDao(book) }?.toList() ?: listOf()
    }

    private fun mapBookToBookDao(book: Book): BookDao {
        return BookDao(
            book.id.value,
            book.name,
            book.isbn,
            book.description,
            book.pageNumber,
            book.releaseDate,
            book.imgUrl,
            AuthorService.mapAuthorsListToAuthorsDaoList(book.authors)
        )
    }

}
