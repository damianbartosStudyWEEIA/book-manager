package pl.djsdpjee

import io.ktor.application.call
import io.ktor.auth.*
import io.ktor.response.respondRedirect
import io.ktor.sessions.SessionStorageMemory
import io.ktor.sessions.Sessions
import io.ktor.sessions.cookie
import pl.djsdpjee.service.UserService


const val AUTHORISATION_FORM_SERVICE: String = "authorisation"
const val AUTHORISATION_SESSION_SERVICE: String = "authorisation_session"
const val AUTHORISATION_COOKIE_NAME: String = "auth_cookie"

internal fun Authentication.Configuration.configureSessionAuth() {
    session<UserIdPrincipal>(AUTHORISATION_SESSION_SERVICE) {
        challenge {
            // What to do if the user isn't authenticated
            call.respondRedirect("/auth/login")
        }
        validate { session: UserIdPrincipal ->
            session
        }
    }
}

internal fun Authentication.Configuration.configureFormAuth() {
    form(AUTHORISATION_FORM_SERVICE) {
        userParamName = "userLogin"
        passwordParamName = "password"
        challenge {
            val errors: Map<Any, AuthenticationFailedCause> = call.authentication.errors
            when (errors.values.singleOrNull()) {
                AuthenticationFailedCause.InvalidCredentials ->
                    call.respondRedirect("")

                AuthenticationFailedCause.NoCredentials ->
                    call.respondRedirect("")

                else ->
                    call.respondRedirect("/auth/login")
            }
        }
        validate { cred: UserPasswordCredential ->
            if (UserService.loginUser(cred.name, cred.password))
                UserIdPrincipal(cred.name)
            else
                null
        }
    }
}

internal fun Sessions.Configuration.configureAuthCookie() {
    cookie<UserIdPrincipal>(
        AUTHORISATION_COOKIE_NAME,
        storage = SessionStorageMemory()
    ) {
        cookie.path = "/"
        cookie.extensions["SameSite"] = "lax"
    }
}
