package pl.djsdpjee.dao

data class UserDao(var userLogin: String, var hashedPassword: String, var books: List<BookDao>){
    override fun toString(): String {
        return "User($userLogin, $hashedPassword, ${books})"
    }
}
