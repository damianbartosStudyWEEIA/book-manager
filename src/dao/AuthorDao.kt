package pl.djsdpjee.dao

import java.util.*

data class AuthorDao(
    var id: UUID = UUID.randomUUID(),
    var firstName: String,
    var lastName: String
)
