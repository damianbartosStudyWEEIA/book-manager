package pl.djsdpjee.dao

import org.joda.time.DateTime
import pl.djsdpjee.service.IsbnService
import java.util.*

data class BookDao(
    var id: UUID = UUID.randomUUID(),
    var name: String = "",
    var isbn: String = IsbnService.getRandomIsbn(),
    var description: String = "",
    var pageNumber: Int = 0,
    var releaseDate: DateTime = DateTime.now(),
    var imgUrl: String = "",
    var authors: List<AuthorDao> = emptyList()
)
