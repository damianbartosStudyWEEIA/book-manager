package pl.djsdpjee.domain

import org.jetbrains.exposed.sql.Table

object BookAuthors: Table() {
    val book = reference("book", Books).primaryKey(0)
    val author = reference("author", Authors).primaryKey(1)
}
