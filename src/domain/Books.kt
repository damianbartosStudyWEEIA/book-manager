package pl.djsdpjee.domain

import org.jetbrains.exposed.dao.UUIDTable

object Books: UUIDTable(){
    val name = varchar("name", 50)
    val isbn = varchar("isbn", 13)
    val description = varchar("description", 500)
    val pageNumber = integer("pageNumber")
    val releaseDate = date("releaseDate")
    val imgUrl = varchar("imgUrl", 500)
}
