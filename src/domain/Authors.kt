package pl.djsdpjee.domain

import org.jetbrains.exposed.dao.UUIDTable

object Authors: UUIDTable(){
    val firstName = varchar("firstName", 50)
    val lastName = varchar("lastName", 50)
}

