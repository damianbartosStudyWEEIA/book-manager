package pl.djsdpjee.domain

import org.jetbrains.exposed.dao.UUIDTable

object Users: UUIDTable() {
    val userLogin = varchar("userLogin", 50)
    val hashedPassword = varchar("hashedPassword", 255)
}
