package pl.djsdpjee.domain

import org.jetbrains.exposed.sql.Table

object UserBooks: Table() {
    val user = reference("user", Users).primaryKey(0)
    val book = reference("book", Books).primaryKey(1)
}
