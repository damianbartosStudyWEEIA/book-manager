package pl.djsdpjee.repository

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import pl.djsdpjee.domain.Authors
import java.util.*

class Author(id: EntityID<UUID>): UUIDEntity(id){
    companion object : UUIDEntityClass<Author>(Authors)

    var firstName by Authors.firstName
    var lastName by Authors.lastName

    override fun toString(): String {
        return "Author($id, $firstName, $lastName)"
    }

}
