package pl.djsdpjee.repository

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import pl.djsdpjee.domain.BookAuthors
import pl.djsdpjee.domain.Books
import java.util.*

class Book(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Book>(Books)

    var name by Books.name
    var isbn by Books.isbn
    var description by Books.description
    var pageNumber by Books.pageNumber
    var releaseDate by Books.releaseDate
    var imgUrl by Books.imgUrl

    var authors by Author via BookAuthors

    override fun toString(): String {
        return "Book($id, $name, $isbn, $description, $pageNumber, $releaseDate, authors: [${authors.joinToString()}])"
    }

}
