package pl.djsdpjee.repository

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import pl.djsdpjee.domain.UserBooks
import pl.djsdpjee.domain.Users
import java.util.*

class User (id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<User>(Users)

    var userLogin by Users.userLogin
    var hashedPassword by Users.hashedPassword

    var books by Book via UserBooks

    override fun toString(): String {
        return "User($id, $userLogin, $hashedPassword, [${books.joinToString()}])"
    }
}
