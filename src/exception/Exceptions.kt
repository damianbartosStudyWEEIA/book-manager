package pl.djsdpjee.exception

class NoSuchUserException(message: String): Exception(message)
class LoginAlreadyExist(message: String): Exception(message)
class PasswordChangeException(message: String): Exception(message)

class BookNotFound(message: String):Exception(message)

class AuthorNotFound(message: String):Exception(message)

class PasswordValidationError(message: String, errors: List<String>): Exception(message)

class NoAccess(message: String): Exception(message)
