package pl.djsdpjee

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import pl.djsdpjee.domain.*
import pl.djsdpjee.service.AuthorService
import pl.djsdpjee.service.BookService
import pl.djsdpjee.service.IsbnService
import pl.djsdpjee.service.UserService

object DataBaseManager {
    private const val DRIVER = "org.h2.Driver"
    private const val NAME = "jdbc:h2:tcp://localhost/~/booksDB"
    private const val PASSWORD = "sa"
    private const val USERNAME = "sa"

    private val db by lazy {
        Database.connect(NAME, DRIVER, USERNAME, PASSWORD)
    }

    fun createSchemas() {
        transaction(db) {
            val tables = arrayOf(Books, Authors, BookAuthors, Users, UserBooks)
            SchemaUtils.drop(*tables)
            SchemaUtils.create(*tables)
//            addLogger(StdOutSqlLogger)
        }
    }

    fun fillDummy() {
        BookService.createBook(bookName = "ksiazka1", bookIsbn = "12cc3")

        val a1 = AuthorService.createAuthor("Adam", "Mickiewicz")
        val a2 = AuthorService.createAuthor("Jan", "Miech")
        val a3 = AuthorService.createAuthor("FName", "LName")
        val a4 = AuthorService.createAuthor("Janek", "Mikolasowikowski")

        val book1 = BookService.createBook(
            bookName = "book1",
            bookIsbn = IsbnService.getRandomIsbn(),
            bookImgUr = "https://static.polityka.pl/_resource/res/path/49/9b/499bebcb-2462-49a3-8a07-5d4e5f97b6b9_830x830",
            bookAuthors = *arrayOf(a1, a2, a3, a4)
        )
        val book2 = BookService.createBook(
            bookName = "kappa",
            bookIsbn = IsbnService.getRandomIsbn(),
            bookAuthors = *arrayOf(a1, a2)
        )

        UserService.registerUser("user1", "test1", "test1")
        UserService.addBooksToCollection("user1", book1, book2)
    }

}
