package pl.djsdpjee.controller

import io.ktor.application.call
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.http.Parameters
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import io.ktor.thymeleaf.ThymeleafContent
import pl.djsdpjee.AUTHORISATION_COOKIE_NAME
import pl.djsdpjee.AUTHORISATION_SESSION_SERVICE
import pl.djsdpjee.exception.NoSuchUserException
import pl.djsdpjee.service.UserService
import pl.djsdpjee.service.validator.PasswordValidator
import pl.djsdpjee.service.validator.PasswordValidatorResult
import pl.djsdpjee.service.validator.UserValidator
import pl.djsdpjee.service.validator.UserValidatorResult


fun Routing.securityController() {
    route("/auth") {
        get("/login") {
            call.respond(ThymeleafContent("login", emptyMap()))
        }
        post("/login") {
            val postParameters: Parameters = call.receiveParameters()
            val login = postParameters["userLogin"]!!
            val password = postParameters["password"]!!

            try {
                if (UserService.loginUser(login, password)) {
                    call.sessions.set(UserIdPrincipal(login))
                    call.respondRedirect("/bookList")
                }
            } catch (e: NoSuchUserException) {}
            call.respond(ThymeleafContent("login", mapOf("errors" to listOf("login error"))))
        }
        get("/logout") {
            call.sessions.clear(AUTHORISATION_COOKIE_NAME)
            call.respondRedirect("/auth/login")

        }

        get("/register") {
            call.respond(ThymeleafContent("register", emptyMap()))
        }
        post("/register") {
            val postParameters: Parameters = call.receiveParameters()
            val login = postParameters["userLogin"]!!
            val password = postParameters["password"]!!
            val rePassword = postParameters["rePassword"]!!

            when (val validateUser = UserValidator.validateUser(login, password, rePassword)) {
                is UserValidatorResult.Error -> call.respond(
                    ThymeleafContent(
                        "register",
                        mapOf(
                            "errors" to validateUser.errors,
                            "login" to login,
                            "password" to password
                        )
                    )
                )
                is UserValidatorResult.Success -> {
                    UserService.registerUser(login, password, rePassword)
                    call.sessions.set(UserIdPrincipal(login))
                    call.respondRedirect("/bookList")
                }
            }
        }

        authenticate(AUTHORISATION_SESSION_SERVICE) {
            get("/passwordChange") {
                call.respond(ThymeleafContent("passwordChange", emptyMap()))
            }
            post("/passwordChange") {
                val login = call.principal<UserIdPrincipal>()!!.name
                val postParameters: Parameters = call.receiveParameters()

                val currentPassword = postParameters["currentPassword"]!!
                val newPassword = postParameters["newPassword"]!!
                val newPasswordRepeated = postParameters["newPasswordRepeated"]!!

                when (val passwordValidation = PasswordValidator.validatePassword(newPassword, newPasswordRepeated)) {
                    is PasswordValidatorResult.Error -> call.respond(
                        ThymeleafContent(
                            "passwordChange",
                            mapOf("errors" to passwordValidation.errors)
                        )
                    )
                    is PasswordValidatorResult.Success -> {
                        try {
                            UserService.changePassword(login, currentPassword, newPassword, newPasswordRepeated)
                            call.respond(
                                ThymeleafContent(
                                    "passwordChange",
                                    mapOf("successes" to listOf("successfully changed password"))
                                )
                            )
                        } catch (e: Exception) {
                            call.respond(ThymeleafContent("passwordChange", mapOf("errors" to listOf(e.message))))
                        }
                    }
                }
            }
        }
    }
}
