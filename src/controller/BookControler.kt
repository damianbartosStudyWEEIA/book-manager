package pl.djsdpjee.controller


import io.ktor.application.call
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.http.Parameters
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.thymeleaf.ThymeleafContent
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import pl.djsdpjee.AUTHORISATION_SESSION_SERVICE
import pl.djsdpjee.dao.AuthorDao
import pl.djsdpjee.dao.BookDao
import pl.djsdpjee.domain.Books
import pl.djsdpjee.exception.AuthorNotFound
import pl.djsdpjee.exception.BookNotFound
import pl.djsdpjee.exception.NoAccess
import pl.djsdpjee.service.AuthorService
import pl.djsdpjee.service.BookService
import pl.djsdpjee.service.UserService
import java.util.*

fun Routing.bookController() {
    authenticate(AUTHORISATION_SESSION_SERVICE) {
        //create
        get("/bookList/create") {
            val login = call.principal<UserIdPrincipal>()!!.name
            val book = BookDao()
            book.authors = listOf(AuthorDao(firstName = "", lastName = ""))
            call.respond(ThymeleafContent("bookEdit", mapOf("login" to login, "book" to book)))
        }

        //read
        get("/bookList") {
            val login = call.principal<UserIdPrincipal>()!!.name
            val books = BookService.getBooksDaoByUserLogin(login)
            call.respond(ThymeleafContent("booksList", mapOf("books" to books, "login" to login)))
        }
        get("/bookList/{id}") {
            val login = call.principal<UserIdPrincipal>()!!.name
            val idCallParameter = UUID.fromString(call.parameters["id"]!!)
            if (!UserService.bookWithIdOwnedByUser(idCallParameter, login)) {
                throw NoAccess("no access to book you dont own")
            }

            val book = BookService.getBookDaoById(idCallParameter)
            call.respond(ThymeleafContent("bookDetails", mapOf("login" to login, "book" to book)))
        }

        //update
        get("/bookList/update/{id}") {
            val login = call.principal<UserIdPrincipal>()!!.name
            val idCallParameter = UUID.fromString(call.parameters["id"]!!)
            if (!UserService.bookWithIdOwnedByUser(idCallParameter, login)) {
                throw NoAccess("no access to book you dont own")
            }

            val book = BookService.getBookDaoById(idCallParameter)
            call.respond(ThymeleafContent("bookEdit", mapOf("login" to login, "book" to book)))
        }
        post("/bookList/update") {
            val postParameters: Parameters = call.receiveParameters()
            val login = call.principal<UserIdPrincipal>()!!.name
            val postBookDao = mapCallToBookDao(postParameters)

            val book = try {
                BookService.editBook(postBookDao)
            } catch (e: BookNotFound) {
                val createdBook = BookService.createBook(postBookDao)
                UserService.addBooksToCollection(login, createdBook)
                createdBook
            }
            call.respondRedirect("/bookList/${book.id}")
        }

        //delete
        get("/bookList/delete/{id}") {
            val login = call.principal<UserIdPrincipal>()!!.name
            val idCallParameter = UUID.fromString(call.parameters["id"]!!)
            if (!UserService.bookWithIdOwnedByUser(idCallParameter, login)) {
                throw NoAccess("no access to book you dont own")
            }

            UserService.removeBookWithIdOwnedByUser(idCallParameter, login)
            BookService.removeBookById(idCallParameter)

            call.respondRedirect("/bookList/")
        }
    }
}

private fun mapCallToBookDao(postParameters: Parameters): BookDao {
    return BookDao(
        id = UUID.fromString(postParameters[Books.id.name]!!),
        isbn = postParameters[Books.isbn.name]!!,
        name = postParameters[Books.name.name]!!,
        pageNumber = postParameters[Books.pageNumber.name]!!.toInt(),
        releaseDate = DateTime.parse(postParameters[Books.releaseDate.name]!!, DateTimeFormat.forPattern("yyyy-MM-dd")),
        description = postParameters[Books.description.name]!!,
        imgUrl = postParameters[Books.imgUrl.name]!!,
        authors = mapAuthorsParametersToAuthorDaoList(postParameters.getAll("authorsFirstName[]")!!, postParameters.getAll("authorsLastName[]")!!)
    )
}

private fun mapAuthorsParametersToAuthorDaoList(firstNameList: List<String>, lastNameList: List<String>):List<AuthorDao>{
    val authors = mutableListOf<AuthorDao>()
    for (i in firstNameList.indices){
        val firstName = firstNameList[i]
        val lastName = lastNameList[i]
        val authorDao = try {
            AuthorService.findAuthor(firstName, lastName)
        }catch (e: AuthorNotFound){
            if(!checkIfListContainsAuthor(firstName, lastName, authors)){
                AuthorDao(firstName = firstName, lastName = lastName)
            }else{
                continue
            }
        }
        authors.add(authorDao)
    }
    return authors
}

private fun checkIfListContainsAuthor(firstName:String, lastName:String, authors:List<AuthorDao>): Boolean {
    for (author in authors) {
        if(author.firstName == firstName && author.lastName==lastName){
            return true
        }
    }
    return false
}
