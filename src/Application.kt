package pl.djsdpjee

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.sessions.Sessions
import io.ktor.thymeleaf.Thymeleaf
import io.ktor.thymeleaf.ThymeleafContent
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import pl.djsdpjee.controller.bookController
import pl.djsdpjee.controller.securityController
import pl.djsdpjee.exception.BookNotFound
import pl.djsdpjee.exception.NoAccess

fun main() {
    embeddedServer(
        Netty, watchPaths = listOf("BookManager"), port = 8080,
        module = Application::booksModule
    ).apply {
        start(wait = true)
    }
}

@Suppress("unused") // Referenced in application.conf
fun Application.booksModule() {

    DataBaseManager.createSchemas()

    //fill for testing
    DataBaseManager.fillDummy()

    install(Thymeleaf) {
        setTemplateResolver(ClassLoaderTemplateResolver().apply {
            prefix = "templates/thymeleaf/"
            suffix = ".html"
            characterEncoding = "utf-8"
        })
    }

    install(Sessions) {
        configureAuthCookie()
    }

    install(Authentication) {
        configureSessionAuth()
        configureFormAuth()
    }

    routing {
        get("") {
            call.respondRedirect("/auth/login")
        }
        bookController()
        securityController()

        static("/static") {
            resources("static")
        }
    }

    // status pages example
    install(StatusPages) {
        exception<BookNotFound> {
            call.respond(ThymeleafContent("error", mapOf("errors" to listOf(it.message))))
        }
        exception<NoAccess> {
            call.respond(ThymeleafContent("error", mapOf("errors" to listOf(it.message))))
        }
    }
}
